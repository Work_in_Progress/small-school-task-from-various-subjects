#!/bin/bash

if [ -n "$1" ];then
echo "No arguments are allowed"
exit 1
fi

touch prihlaseni.txt
echo "UZIVATEL  POCET PRIHLASENI" > prihlaseni.txt
echo "ursulale  $(last ursulale | egrep -v "Dec 1[8-9]" | egrep -v "Dec 2[0-9]" | wc -l)" >> prihlaseni.txt
printf "\n" >> prihlaseni.txt

touch tempfile.txt

for user in $(ypcat passwd | egrep -i ':alexandr ' | cut -d ":" -f1 | grep -v "$(whoami)");do
count="$(last $user | egrep -v "Dec 1[8-9]" | egrep -v "Dec 2[0-9]" | wc -l)"
if [ $count -lt 1 ];then
continue
fi
echo "$user     $count" >> tempfile.txt

done

length="$(( $(wc -l < tempfile.txt) ))"
cat tempfile.txt | sort -k2nr > x;
mv x tempfile.txt
for ((i=1;i<=length;i++));do

cat tempfile.txt | head -n $i | tail +$i >> prihlaseni.txt

done

rm -rf tempfile.txt
exit 0
