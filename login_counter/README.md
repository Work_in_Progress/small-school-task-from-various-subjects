This is a homework task quotation given by my university translated to english
to the best of my ability.

Write a script that extracts users with the same name as you.
Name is defined as the first word of the fifth column in ypcat passwd.

Find out how many times they have logged in to <#schoolserver> from the moment
of it's installation to midnight between 17th December and 18th December.

Print out results in the form of a table as shown below. You should always
occupy the first row and be separated with one blank row. Print out the list
of users (excluding yourself) by the descending amount of logins. Also exclude 
users who haven't logged in at all. The script will be called "prihlaseni.sh" and
the table it creates will be written in a file called "prihlaseni.txt".

#EXAMPLE TABLE

UZIVATEL      POCET PRIHLASENI
you                580

yyy               1000
zzz                102
zxz                  3
