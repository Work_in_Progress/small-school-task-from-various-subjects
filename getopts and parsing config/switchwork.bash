#!/bin/bash

VERBOSE=0
DEBUG=0

while getopts vhd opt
do
	case "$opt" in
		h) echo "READ THE SOURCE CODE."; exit 0;;
		v) VERBOSE=1;;
		d) DEBUG=1;;
		\?) echo -e "\"-v\" is verbose \n\"-d\" is debug mode \n\"-h\" is help" 1>&2; exit 2;;
	esac
done

declare -a FILES

for file in $@
do
	if [[ "$file" =~ ^-.* ]]
	then
		continue
	else
		FILES+=("$file")
	fi
done

declare -a CONF_SETTINGS
declare -a CONF_VALUES

for file in ${FILES[@]}
do
	CONF_SETTINGS+=($(cat "$file" | sed '/#/d' | egrep '[[:space:]]*.+' | tr -s ' ' | cut -d " " -f2))
	CONF_VALUES+=($(cat "$file" | sed '/#/d' | egrep '[[:space:]]*.+' | tr -s ' ' | cut -d " " -f3))
done

if [ $VERBOSE -eq 1 ]
then
	for conf_index in ${!CONF_SETTINGS[@]}
	do
		printf "[%s] = [%s]\n" "${CONF_SETTINGS[$conf_index]}" "${CONF_VALUES[$conf_index]}"
	done
fi
