#!/bin/bash
#takes a file path as the first input, more info in the script description next
#to this file.
OIFS="$IFS"
IFS=$'\n'
if [ -n "$2" ];then
        echo "Too many arguments."
        exit 1
fi
if [ ! -d "$1" ];then
        echo "Not a directory or not a full path to the directory."
        exit 1
fi

for file in $(ls "$1");do
        extension="$(echo "$file" | cut -d"." -f2)"
        mkdir files-"$extension" 2>/dev/null
        mv "$1"/"$file" files-"$extension" 2>/dev/null
done
IFS="$OIFS"
exit 0
