I did a homework assignment from my university, made it a little bit modular,
now it takes a file as an input and can interact if a person does something
wrong. The homework only worked with a specific file.
This script takes a directory path or just its name if the directory is located
in the same parent directory as you. It searches the directory for files with
extensions at the end, for instance .avi .tor .docx .jar and so on, it creates
a directory with extension name for each unique extension it finds and moves the
file from the inputted directory to a new directory named after its extension.
In essence it sorts a box of files by their extension and puts each file in
the right box.