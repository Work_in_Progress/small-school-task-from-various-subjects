# HTML manipulation script

Homework given by my university CVUT FIT, specifically from the subject
Programming in Shell 2.

The goal is to parse HTML code and extract subject codes, make directories from
them, put files named index containing "=====subject code=====" inside them
and then add an info file containing the code of the current semester.

This works as a ground for backing up. The backup is then produced afterwards.