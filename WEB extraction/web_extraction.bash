#!/bin/bash

if [ $# -ne 0 ];
then
	echo "This scripts takes no arguments."
	exit 1
fi

if [ -d ~/courses -a -n ~/courses -o -d ~/archive -a -n ~/archive ];
then
	echo "A directory ~/courses and/or ~/archive is already in use."
	echo "Do you wish to override the directories? (y/n)"
    	read response 
	if [ "$response" == "y" ];
	then
		echo "Overriding and executing, please wait..."
		rm -rf ~/courses
		rm -rf ~/archive
	else 
		if [ "$response" == "n" ];
		then
			echo "No changes are made, exitting..."
			exit 1
		fi

	echo "Unknown response, exitting..."
	exit 1;
	fi
fi

mkdir ~/courses 1>&2 2>/dev/null

mkdir ~/archive 1>&2 2>/dev/null

curl -s https://edux.fit.cvut.cz/courses/ -o ~/HTML.src

SUBJECTS=$(cat ~/HTML.src | cut -d '>' -f3 | cut -d '/' -f1 | egrep '^[[:upper:]]')

for subject in $SUBJECTS;
do
	mkdir ~/courses/"$subject"
done

SUBJECTS_Q=$(ls ~/courses)

for subject_q in $SUBJECTS_Q;
do
	touch ~/courses/"$subject_q"/index
	echo "=====$subject_q=====" > ~/courses/"$subject_q"/index
done

YEAR_2DG=$(date +%y)

MONTH=$(date +%m)

if [ $MONTH -gt 2 ];
then
	SEMESTER="2"
else
	SEMESTER="1"
fi


SEMESTER_CODE=$(echo "B$(($YEAR_2DG - 1))$SEMESTER")

echo "$SEMESTER_CODE" > ~/courses/info

SEM_TIMESTAMP="$HOME/archive/"$SEMESTER_CODE"@$(date +%Y-%m-%d_%H:%M:%S)"

mkdir "$SEM_TIMESTAMP"

cp -R $HOME/courses/* "$SEM_TIMESTAMP"

find "$SEM_TIMESTAMP" -maxdepth 1 -type f -exec rm  {} \;


rm $HOME/HTML.src

echo "DONE"
