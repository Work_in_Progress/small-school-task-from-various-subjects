I just wanted to do some practice for a test and thought of a great exercise.
I've been writing down my thoughts to sharpen my ideas and writing skills as
of late and wondered how many words I'd written. I came up with a small script
idea where I could count words from all files in a directory that start with a
uniform string with just a few keystrokes.