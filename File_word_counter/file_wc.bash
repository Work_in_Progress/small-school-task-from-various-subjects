#!/bin/bash
#This script counts words in all files starting with a certain string in the current directory.
#The input of this script will be a string which will be considered a starting string of the files you wish to count words in.
word_counter=0
files=0
if [ -n "$2" ]; then
        echo "Too many arguments."
        exit 1
fi
if [ $# -eq 0 ];then #Could also be -z "$1".
        echo "No arguments passed to the script."
        exit 1
fi
for file in $(ls $1* 2>/dev/null); do
        if [ -f "$file" -a -r "$file" ]; then
        word_counter="$((word_counter + $(wc -w < $file) ))"
        files=$((files + 1))
        fi
done;
if ((files == 0)); then
        echo "No files starting with the string \"$1\" were found."
        exit 1
fi
echo "Contents of readable files in this directory starting with inputted string amount to $word_counter words."
exit 0
