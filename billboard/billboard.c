#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define ARRAY_MAX 1000000

void swap(int *px, int *py)
{
    int tmp;
    tmp = *px;
    *px = *py;
    *py = tmp;
}
void bubble_sort(int arr[], int n)
{
    int i, j;
    for (i = 0; i < n - 1; i++)
    {
        for (j = 0; j < n - i - 1; j++)
        {
            if (arr[j] > arr[j + 1])
                swap(&arr[j], &arr[j + 1]);
        }
    }
}

int main(void)
{
    int len;                         // The distance of the highway
    char char1;                      // should be ':'
    char char3;                      // should be ',' or '}'
    char char4;                      // should be '{'
    char char5 = 'p';                // should be '}'
    int amount_of_pot_bilboards = 0; // How many positions has the user set.
    int bilboard_spots[ARRAY_MAX];   // an array of all the positions the user has set.
    int position = 0;                // the bilboard number that we currently occupy, default is 1;
    int dist;                        // the distance we can leap in one jump
    int result_amount = 0;           // The minimum resulting amount of bilboard spots we had to use, default is 0
    int remembered_position;         // The position we last occupied
    int truth1 = 1;                  // deus ex machina1
    int truth2 = 1;                  // deus ex machina2
    printf("Mozna umisteni:\n");
    scanf("%d %c", &len, &char1);
    if (char1 != ':' || 0 >= len)
    {
        printf("Nespravny vstup.\n");
        return 0;
    }
    scanf(" %c", &char4);
    if (char4 != '{')
    {
        printf("Nespravny vstup.\n");
        return 0;
    }
    int i = 0;
    while (!feof(stdin))
    {
        if (amount_of_pot_bilboards + 1 > 1000000)
        {
            printf("Nespravny vstup.\n");
            return 0;
        }
        if (scanf("%d %c", &bilboard_spots[i], &char3) && (char3 == ',' || char3 == '}'))
        {
            if ((len - 1) < bilboard_spots[i] || bilboard_spots[i] < 1)
            {
                printf("Nespravny vstup.\n");
                return 0;
            }
            i++;
            amount_of_pot_bilboards++;
        }
        else
        {
            printf("Nespravny vstup.\n");
            return 0;
        }
        if (char3 == '}')
        {
            char5 = '}';
            break;
        }
        if (!bilboard_spots[0])
        {
            printf("Nespravny vstup.\n");
            return 0;
        }
    }
    if (char5 != '}')
    {
        printf("Nespravny vstup.\n");
        return 0;
    }

    bubble_sort(bilboard_spots, amount_of_pot_bilboards);

    printf("Vzdalenosti:\n");
    while (!feof(stdin))
    {
        if ((scanf(" %d", &dist) == 1) && dist > 0)
        {
            for (int i = 0; i < amount_of_pot_bilboards - 1; i++)
            {
                if ((bilboard_spots[i + 1] - bilboard_spots[i]) > dist)
                {
                    truth1 = 0;
                    break;
                }
            }
            for (int i = 0; i < amount_of_pot_bilboards; i++)
            {
                if ((position + dist) >= len)
                {
                    break;
                }
                if ((dist < bilboard_spots[0]) || (bilboard_spots[amount_of_pot_bilboards - 1] + dist < len))
                {
                    truth2 = 0;
                    break;
                }
                if (truth1 == 0)
                {
                    break;
                }
                if ((position + dist) >= bilboard_spots[i])
                {
                    remembered_position = bilboard_spots[i];
                    if (!bilboard_spots[i + 1])
                    {
                        position = remembered_position;
                        result_amount++;
                    }
                }
                else
                {
                    position = remembered_position;
                    result_amount++;
                    i--;
                }
            }
        }
        else if (feof(stdin))
        {
            return 0;
        }
        else
        {
            printf("Nespravny vstup.\n");
            return 0;
        }
        if (truth1 == 0 || truth2 == 0)
        {
            printf("N/A\n");
        }
        else
        {
            printf("Billboardu: %d\n", result_amount);
        }
        position = 0;
        result_amount = 0;
        remembered_position = 0;
        truth1 = 1;
        truth2 = 1;
    }
    return 0;
}
