_Note that this is a homework task given by my university's Programming and Algorithmics course and this is my solution to it._
_The original task was given in czech language and this is my translation with accuracy to the best of my ability. The solution is meant to be written in C._

_Note that this is representative of my knowledge and ability in C programming language at that point in time with the resources and knowledge given by the university._

# The goal is to write a program which is going to plan the placement of billboards along a highway.

Assume a highway with a total length `len`. Places are chosen along the highway where you can place billboards (you cannot place them anywhere else).
These places are given by their distance from the beginning of the highway, a distance is a natural number
from the interval <1;len-1>. Overall there can be a maximum of 1 000 000 places.
Some of these places are going to be chosen to errect a billboard. It is necessary to place as few billboards as possible, but at the same time
you must ensure that between any two consecutive billboards the distance will not exceed `dist`.
Also the first billboard must not be placed further than `dist` from the beginning of the highway while the last must not be placed
further than `dist` from the end of the highway.
Based on this information your program is going to calculate how many billboards we need to place.

Input of the program will be length of the highway `len` (natural number) followed by the list of potential positions for billboard placement.
Position of the potential billboard will be a natural number and will be written within curly brackets with delimiter ','
Requests will follow after potential billboard positions. Each request is a natural number `dist`, this number declares the maximum acceptable distance
between two consecutive billboards. Entering of the requests is terminated when you reach the end of the input (EOF).

Output of the program is calculation of the minimal necessary amount of billboards for each requests, potentially
the program will display an error message depending on the reason.
The accurate format of the output is shown in the showcase below.

Error messages will be displayed in standard output.

An error is considered to be:

* The distance is not a valid natural number.
* Length of the highway is either a negative number or a 0.
* The position for billboard placement isn't within the interval <1;len-1>
* No positions for billboard placement are given.
* More than 1 000 000 positions for billboard placement are given.
* Requested distance `dist` is negative or 0.
* A delimiter is either extra or missing.

_Note that I did not translate the showcase as it corresponds with my solution and I would have to translate my solution as well in that case_

Showcase of the program:

Mozna umisteni:
1000: { 250, 500, 750 }
Vzdalenosti:
800
Billboardu: 1
500
Billboardu: 1
300
Billboardu: 3
250
Billboardu: 3
200
N/A

Mozna umisteni:
1000 : { 250 , 300 , 550 , 750 }
Vzdalenosti:
371
Billboardu: 3
507
Billboardu: 2
273
Billboardu: 4
561
Billboardu: 1

Mozna umisteni:
1000:{481,692,159,843,921,315}
Vzdalenosti:
1000
Billboardu: 0
999
Billboardu: 1
519
Billboardu: 1
518
Billboardu: 2
377
Billboardu: 2
376
Billboardu: 3
315
Billboardu: 3
314
Billboardu: 4
308
Billboardu: 4
307
Billboardu: 5
211
Billboardu: 5
210
N/A

Mozna umisteni:
3:{1,2,1,2}
Vzdalenosti:
1
Billboardu: 2
10
Billboardu: 0

Mozna umisteni:
500:{250,830}
Nespravny vstup.

Mozna umisteni:
330:{15,240 310
Nespravny vstup.
