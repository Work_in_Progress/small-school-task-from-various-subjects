#!/bin/sed -nf
/<h3/ {
  s/.*Platnost pro //
  s/P.*: /#/
  s/<.*//
  p
}
/<table.*kurzy_tisk/,+2 {
  s/<\/tr>/\n/g
  s/<\/t[hd]>/|/g
  s/<[^>]*>//g
  s/|\n/\n/g
  s/\r//
  s/\n$//
  /^$/!p
}

