#!/usr/bin/perl

my $last_exit = $?; 

if (not defined ($ARGV[0]) or not defined ($ARGV[1]))
{
	print($last_exit);
	exit 2;
}
$text = $ARGV[0];
$re = $ARGV[1];
@positions;
while($text =~ m/$re/g)
{
$num = pos($text) - length($re);
push @positions, $num;
}
$i = 0;
$i_max = scalar @positions - 1;
if(!@positions)
{
	print("pattern not found.\n");
	print($last_exit);
	exit 1;
}
print($text, "\n");
for (my $x = 0; $x < length($text); $x++)
{
	if ($x != @positions[$i])
	{
		print(" ");
	}else
	{
		print("^");
		if($i != $i_max)
		{
			$i++;
		}else
		{
			print("\n");
			print($last_exit);
			last;
		}
	}
}
